<?php

namespace LDSScriptures;

require_once __DIR__.'/vendor/autoload.php';

// Callback filter functions
function filter_chapters_or_verses($value)
{
    if ($value === 'chapters' || $value === 'verses') {
        return $value;
    }

    return Config::DEFAULT_CHAPTERS_OR_VERSES;
}

// Filter input
$args = [
    'goal' => [FILTER_SANITIZE_STRING, 'options' => ['filter_chapters_or_verses']],
    'start_chapter_id' => [
        FILTER_SANITIZE_NUMBER_INT,
        'options' => ['min_range' => Config::OT_CHAPTERS_FIRST_ID, 'max_range' => Config::PGP_CHAPTERS_LAST_ID],
    ],
    'end_chapter_id' => [
        FILTER_SANITIZE_NUMBER_INT,
        'options' => ['min_range' => Config::OT_CHAPTERS_FIRST_ID, 'max_range' => Config::PGP_CHAPTERS_LAST_ID],
    ],
    'start_date' => FILTER_SANITIZE_STRING,
    'end_date' => FILTER_SANITIZE_STRING,
    'date' => FILTER_SANITIZE_STRING,
    'chapters_or_verses' => [
        FILTER_SANITIZE_STRING,
        'options' => ['filter_chapters_or_verses']
    ],
    'num_per_day' => FILTER_SANITIZE_NUMBER_INT,
    'highlight_shortest_verse' => FILTER_VALIDATE_BOOLEAN,
    'highlight_shortest_number' => FILTER_SANITIZE_NUMBER_INT,
    'format' => FILTER_SANITIZE_STRING,
];

$schedule = new Logic;
$schedule->create(filter_input_array(INPUT_GET, $args));
