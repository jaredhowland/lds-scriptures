# LDS Scriptures #

This is a project I started to experiment with programming. I am interested in learning more about [PHP][], [OOP][] in general, [HTML5][], [SASS][]/[Compass][], and [SQLite][]. This seemed like a good project to combine all of those interests into something I will potentially use on a regular basis. Some of the features I hope to include are:

1. *Scripture Reading Scheduler*: Create a scripture reading schedule. Set either time-based or length-based reading goals. Subscribe to schedule with an ATOM or RSS feed.
2. *Reading*: Create a pleasant reading experience with nice typography and a responsive layout for various sized screens (laptops, tablets, phones).

I am using the [Twig template engine][] for the front-end and the SQLite version (v3.0 rc2) of the [LDS Scriptures][] from the [Mormon Documentation Project][] (slightly modified).

**This project is not affiliated with, nor endorsed by, the [LDS Church].**

[PHP]: http://php.net/
[OOP]: http://en.wikipedia.org/wiki/Object-oriented_programming
[HTML5]: http://www.whatwg.org/specs/web-apps/current-work/multipage/
[SASS]: http://sass-lang.com/
[Compass]: http://compass-style.org/
[SQLite]: http://www.sqlite.org/
[Twig template engine]: http://twig.sensiolabs.org/
[LDS Scriptures]: http://www.lds.org/scriptures
[Mormon Documentation Project]: http://scriptures.nephi.org/
[LDS Church]: http://www.lds.org