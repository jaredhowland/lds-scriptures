<?php
/**
 * Class to create reading schedule using a time-based goal
 *
 * @author  Jared Howland <scriptures@jaredhowland.com>
 * @version 2019-11-18
 * @since   2012-12-13
 *
 */

namespace LDSScriptures;

use Exception;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class ScheduleTime extends Schedule
{
    private $date;
    private $end_date;
    private $max_end_date;

    /**
     * Time-based schedule constructor.
     *
     * @param array $args Array of arguments needed to construct the `scheduleTime` object
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function __construct($args)
    {
        // Common variables to pass to parent constructor in schedule class
        $goal                      = $args['goal'];
        $start_chapter_id          = $args['start_chapter_id'];
        $end_chapter_id            = $args['end_chapter_id'];
        $start_date                = $args['start_date'];
        $highlight_shortest_verse  = $args['highlight_shortest_verse'];
        $highlight_shortest_number = $args['highlight_shortest_number'];
        // Time-based schedule variables
        $date     = $args['date'];
        $end_date = $args['end_date'];
        // Pass arguments to schedule class
        parent::__construct($goal, $start_chapter_id, $end_chapter_id, $start_date, $highlight_shortest_verse, $highlight_shortest_number);
        // Set variables for child class scheduleTime
        $this->set_max_end_date();
        $this->set_end_date($end_date);
        $this->set_date($date);
    }

    /**
     * Create a time-based reading schedule
     *
     * @return array Array containing metadata for the reading schedule
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function time_based_schedule(): array
    {
        $schedule       = $this->create_reading_schedule();
        $schedule_key   = $this->get_key($schedule);
        $date_schedule  = $schedule[$schedule_key];
        $scriptures     = new scriptures;
        $first_verse_id = $scriptures->get_first_verse_id($date_schedule['first_chapter_id']);
        $last_verse_id  = $scriptures->get_last_verse_id($date_schedule['last_chapter_id']);
        try {
            $schedule = $scriptures->reading($first_verse_id, $last_verse_id, $this->get_highlight_shortest_verse(), $this->get_highlight_shortest_number());
        } catch (Exception $e) {
            echo 'Error: ', $e->getMessage();
        }
        $next         = $this->get_next($this->date, $this->end_date);
        $prev         = $this->get_prev($this->date, $this->end_date);
        $url_schedule = Config::url();
        $url_date     = $url_schedule.'&amp;date='.$this->date;
        $updated      = $this->get_atom_date($this->date);

        return array(
            'next_url' => $next,
            'prev_url' => $prev,
            'url_schedule' => $url_schedule,
            'url_date' => $url_date,
            'updated' => $updated,
            'schedule' => $schedule,
        );
    }

    /**
     * Sets the farthest out date possible for the reading schedule.
     */
    private function set_max_end_date()
    {
        $max_days           = (count((array)$this->get_chapter_verse_counts())) - 1;
        $this->max_end_date = date('Y-m-d', strtotime('+'.$max_days.' days', strtotime($this->get_start_date())));
    }

    /**
     * Validates and sets the $this->end_date variable
     *
     * @param string End date for reading schedule.
     */
    private function set_end_date($end_date)
    {
        $end_date = date('Y-m-d', strtotime($end_date, strtotime($this->get_start_date())));
        if ($end_date === null || strtotime($end_date) > strtotime($this->max_end_date) || strtotime($end_date) <= strtotime($this->get_start_date())) {
            $this->end_date = $this->max_end_date;
        } else {
            $this->end_date = $end_date;
        }
    }

    /**
     * Validates and sets the $this->date variable
     *
     * @param string $date Date to use in calculating reading schedule. Default: `Config::DEFAULT_DATE`.
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    private function set_date($date = null)
    {
        if (empty($date)) {
            $date = date('Y-m-d', strtotime(Config::DEFAULT_DATE));
        }
        $this->date = date('Y-m-d', strtotime($date));
        if (strtotime($this->date) < strtotime($this->get_start_date())) {
            $error = 'It appears you are trying to see your reading schedule for '.date('F j, Y',
                        strtotime($this->date)).'. However, this schedule was not set to begin until '.date('F j, Y',
                        strtotime($this->get_start_date())).'.';
            Template::display('error.tmpl', [$error]);
            die();
        }

        if (strtotime($this->date) > strtotime($this->end_date)) {
            $error = 'It appears you are trying to see your reading schedule for '.date('F j, Y',
                        strtotime($this->date)).'. However, this schedule was set to end '.date('F j, Y',
                        strtotime($this->end_date)).'.';
            Template::display('error.tmpl', [$error]);
            die();
        }
    }

    /**
     * Used to create the reading schedule when it is a time-based goal
     *
     * @return array Array containing the reading schedule
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    private function create_reading_schedule(): array
    {
        $min             = 0;
        $max             = 10000;
        $guess           = round(($min + $max) / 2);
        $schedule        = $this->set_schedule($guess);
        $actual_end_date = $this->get_actual_end_date($schedule);
        while (strtotime($actual_end_date) !== strtotime($this->end_date)) {
            if (strtotime($actual_end_date) < strtotime($this->end_date)) {
                $min = $guess;
            } elseif (strtotime($actual_end_date) > strtotime($this->end_date)) {
                $max = $guess;
            }
            $guess = round(($min + $max) / 2);
            // Sometimes you cannot create a reading schedule that ends exactly on the desired date
            // If that is the case, make the end date one day before the original target end date then create the schedule
            if ($guess === $max || $guess === $min) {
                $min            = 0;
                $max            = 10000;
                $this->end_date = date('Y-m-d', strtotime('-1 day', strtotime($this->end_date)));
                // Must set date again to make sure appropriate error messages are displayed for date ranges outside the new range
                $this->set_date($this->date);
            }
            $schedule        = $this->set_schedule($guess);
            $actual_end_date = $this->get_actual_end_date($schedule);
        }

        return $schedule;
    }

    /**
     * Creates a reading schedule
     *
     * @param int Guess for number of days to add to start date to get desired end date
     *
     * @return array Array of reading schedule
     */
    private function set_schedule($guess): array
    {
        $end_date = date('Y-m-d', strtotime('+'.$guess.' days', strtotime($this->get_start_date())));

        return $this->calculate_schedule($this->get_target_verses_per_day($this->get_start_date(), $end_date));
    }

    /**
     * Gets the actual end date for a reading schedule
     *
     * @param array Array of reading schedule
     *
     * @return string Actual end date of reading schedule
     */
    private function get_actual_end_date($schedule): string
    {
        $actual_end_date = end($schedule);

        return $actual_end_date['date'];
    }

    /**
     * Used to calculate the reading schedule when it is a time-based goal
     *
     * @param int Target number of verses to read per day
     *
     * @return array Array containing the reading schedule
     */
    private function calculate_schedule($target_verses_per_day): array
    {
        // Initialize variables to be used in foreach loop
        $overage     = 0;
        $verse_count = 0;
        $schedule    = array();
        $date        = date('Y-m-d', strtotime('-1 day', strtotime($this->get_start_date())));
        foreach ((array)$this->get_chapter_verse_counts() as $chapters) {
            $chapter_id = $chapters['chapter_id'];
            $num_verses = $chapters['num_verses'];
            // Define first chapter id of the reading schedule
            // Define date of the day’s schedule being created
            if ($verse_count === 0) {
                $first_chapter_id = $chapter_id;
                $date             = date('Y-m-d', strtotime('+1 day', strtotime($date)));
            }
            $verse_count = $num_verses + $verse_count;
            // If we have equaled or gone over our target verses per day, add first and last chapter ids to schedule array to create the day’s schedule
            if ((($verse_count + $overage) >= $target_verses_per_day) || $chapter_id === $this->get_end_chapter_id()) {
                $schedule[]  = array(
                    'date' => $date,
                    'first_chapter_id' => $first_chapter_id,
                    'last_chapter_id' => $chapter_id,
                );
                $overage     = $verse_count - $target_verses_per_day;
                $verse_count = 0;
            }
        }

        return $schedule;
    }

    /**
     * Returns the target number of verses to read in a day
     *
     * @param string Start date
     * @param string End date
     *
     * @return int Target number of verses that need to be read on average
     */
    private function get_target_verses_per_day($start_date, $end_date): int
    {
        // Total days in the reading schedule
        $days_count = $this->calculate_days_between_dates($start_date, $end_date);
        // Flatten array to get the total number of verses
        $total_verses = $this->get_total_verses();

        return $total_verses / $days_count;
    }

    /**
     * Gets the appropriate key from the schedule array for the day’s reading schedule
     *
     * @param array Schedule array
     *
     * @return int|null Key of day’s reading schedule. Returns first key [(int) 0] if not found.
     */
    private function get_key($schedule)
    {
        foreach ($schedule as $key => $value) {
            if ($value['date'] === $this->date) {

                return $key;
            }
        }

        return 0;
    }
}
