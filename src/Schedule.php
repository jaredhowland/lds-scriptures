<?php
/**
 * Class to create reading schedule
 *
 * @author  Jared Howland <scriptures@jaredhowland.com>
 * @version 2019-10-14
 * @since   2012-12-08
 *
 */

namespace LDSScriptures;

// TODO: remove all references to Volume ID because it is not necessary—limits functionality

use DateTime;
use Exception;

abstract class Schedule
{
    // Define variables. If not provided, default values are used from the Config file.
    // All are optional but may not behave as expected if not set.
    private $goal; // Optional. Valid values include 'time' and 'length'.
    private $start_chapter_id; // Optional. Chapter to start reading.
    protected $end_chapter_id; // Optional. Chapter to end reading.
    protected $start_date; // Optional. Day to start reading schedule.
    private $highlight_shortest_verse; // Optional. Whether or not to highlight shortest verse in reading.
    private $highlight_shortest_number; // Optional. Number of consecutive shortest verses to highlight.
    protected $chapter_verse_counts; // Total number of verses by chapter id.
    private $total_verses; // Total number of verses in reading.

    /**
     * Schedule constructor.
     *
     * @param string $goal                      Reading goal. Valid values: `chapter` or `verses`.
     * @param int    $start_chapter_id          Chapter ID to start the reading schedule with
     * @param int    $end_chapter_id            Chapter ID to end the reading schedule with
     * @param string $start_date                Date to start reading
     * @param bool   $highlight_shortest_verse  Whether or not to highlight the shortest verse(s)
     * @param int    $highlight_shortest_number The number of shortest consecutive verses to highlight
     */
    protected function __construct(
        $goal,
        $start_chapter_id,
        $end_chapter_id,
        $start_date,
        $highlight_shortest_verse,
        $highlight_shortest_number
    ) {
        $this->set_goal($goal);
        $this->set_start_chapter_id($start_chapter_id);
        $this->set_end_chapter_id($end_chapter_id);
        $this->set_start_date($start_date);
        $this->set_highlight_shortest_verse($highlight_shortest_verse);
        // Common methods
        $this->set_chapter_verse_counts();
        $this->set_total_verses();
        $this->set_highlight_shortest_number((int)$highlight_shortest_number);
    }

    /**
     * Retrieves value of the $this->goal variable
     *
     * @return string Reading goal. Valid values: `time` and `length`.
     */
    protected function get_goal(): string
    {
        return $this->goal;
    }

    /**
     * Retrieves value of the $this->start_chapter_id variable
     *
     * @return int Chapter ID. Valid values depend on which volume is selected. Reverts to defaults in Config file if invalid.
     */
    protected function get_start_chapter_id(): int
    {
        return $this->start_chapter_id;
    }

    /**
     * Retrieves value of the $this->end_chapter_id variable
     *
     * @return int Chapter ID. Valid values depend on which volume is selected. Reverts to defaults in Config file if invalid.
     */
    protected function get_end_chapter_id(): int
    {
        return $this->end_chapter_id;
    }

    /**
     * Retrieves value of the $this->start_date variable
     *
     * @return string Start date of reading schedule.
     */
    protected function get_start_date(): string
    {
        return $this->start_date;
    }

    /**
     * Retrieves value of the $this->highlight_shortest_verse variable
     *
     * @return bool Whether or not to highlight shortest verse.
     */
    protected function get_highlight_shortest_verse(): bool
    {
        return $this->highlight_shortest_verse;
    }

    /**
     * Retrieves value of the $this->highlight_shortest_verse variable
     *
     * @return bool Whether or not to highlight shortest verse.
     */
    protected function get_highlight_shortest_number(): bool
    {
        return $this->highlight_shortest_number;
    }

    /**
     * Retrieves value of the $this->chapter_verse_counts variable
     *
     * @return array Total number of verses in reading schedule by chapter
     */
    protected function get_chapter_verse_counts(): array
    {
        return $this->chapter_verse_counts;
    }

    /**
     * Returns the total number of verses in the full schedule
     *
     * @return int Total number of verses
     */
    protected function get_total_verses(): int
    {
        return $this->total_verses;
    }

    /**
     * Calculates the number of days between two dates
     *
     * @param string Start date
     * @param string End date
     *
     * @return int Number of days between any 2 given dates
     */
    protected function calculate_days_between_dates($start_date, $end_date): int
    {
        return floor((strtotime($end_date) - strtotime($start_date)) / (60 * 60 * 24)) + 1;
    }

    /**
     * Creates the next link URL
     *
     * @param string $date     Current date (or date you want to see the reading schedule for).
     * @param string $end_date End date for reading schedule.
     *
     * @return null|string Null if at the end of the reading schedule. String of the next URL otherwise.
     */
    protected function get_next($date, $end_date)
    {
        if ($date === $end_date) {
            return null;
        }

        $tomorrow = date('Y-m-d', strtotime('+1 day', strtotime($date)));
        $highlight_shortest_verse = $this->highlight_shortest_number === true ? 1 : 0;

        return '?goal='.$this->goal.'&amp;start_date='.$this->start_date.'&amp;end_date='.$end_date.'&amp;date='.$tomorrow.'&amp;start_chapter_id='.$this->start_chapter_id.'&amp;end_chapter_id='.$this->end_chapter_id.'&amp;highlight_shortest_verse='.$highlight_shortest_verse.'&amp;highlight_shortest_number='.$this->highlight_shortest_number;
    }

    /**
     * Creates the previous link URL
     *
     * @param string $date     Current date (or date you want to see the reading schedule for).
     * @param string $end_date End date for reading schedule.
     *
     * @return null|string Null if at the beginning of the reading schedule. String of the previous URL otherwise.
     */
    protected function get_prev($date, $end_date)
    {
        if ($date === $this->start_date) {
            return null;
        }

        $yesterday                = date('Y-m-d', strtotime('-1 day', strtotime($date)));
        $highlight_shortest_verse = $this->highlight_shortest_number === true ? true : false;

        return '?goal='.$this->goal.'&amp;start_date='.$this->start_date.'&amp;end_date='.$end_date.'&amp;date='.$yesterday.'&amp;start_chapter_id='.$this->start_chapter_id.'&amp;end_chapter_id='.$this->end_chapter_id.'&amp;highlight_shortest_verse='.$highlight_shortest_verse.'&amp;highlight_shortest_number='.$this->highlight_shortest_number;
    }

    /**
     * Gets the date for the atom feed
     *
     * @param string $date Date of atom feed
     *
     * @return string Date formatted for atom feed
     *
     * @throws Exception if invalid DateTime object
     */
    protected function get_atom_date($date): string
    {
        $date = new DateTime($date);

        return date_format($date, DATE_ATOM);
    }

    /**
     * Sets the $this->goal variable
     *
     * @param string Reading goal. Valid values are 'time' and 'length'.
     */
    private function set_goal($goal)
    {
        // Fall back to default goal if not set correctly
        if ($goal !== 'time' && $goal !== 'length') {
            $this->goal = Config::DEFAULT_GOAL;
        } else {
            $this->goal = $goal;
        }
    }

    /**
     * Validates and sets the $this->start_chapter_id and $this->end_chapter_id variables
     *
     * @param int Chapter ID. Valid values depend on which volume is selected. Reverts to defaults in Config file if invalid.
     */
    private function set_start_chapter_id($start_chapter_id)
    {
        if (!$start_chapter_id || $start_chapter_id < Config::OT_CHAPTERS_FIRST_ID || $start_chapter_id > Config::PGP_CHAPTERS_LAST_ID) {
            $this->start_chapter_id = Config::DEFAULT_START_CHAPTER_ID;
        } else {
            $this->start_chapter_id = (int)$start_chapter_id;
        }
    }

    /**
     * Validates and sets the $this->start_chapter_id and $this->end_chapter_id variables
     *
     * @param int Chapter ID. Valid values depend on which volume is selected. Reverts to defaults in Config file if invalid.
     */
    private function set_end_chapter_id($end_chapter_id)
    {
        if (!$end_chapter_id) {
            $this->start_chapter_id = Config::DEFAULT_END_CHAPTER_ID;
        } elseif ($end_chapter_id < $this->start_chapter_id || $end_chapter_id > Config::PGP_CHAPTERS_LAST_ID) {
            if ($this->start_chapter_id >= Config::OT_CHAPTERS_FIRST_ID && $this->start_chapter_id <= Config::OT_CHAPTERS_LAST_ID) {
                $this->end_chapter_id = Config::OT_CHAPTERS_LAST_ID;
            } elseif ($this->start_chapter_id >= Config::NT_CHAPTERS_FIRST_ID && $this->start_chapter_id <= Config::NT_CHAPTERS_LAST_ID) {
                $this->end_chapter_id = Config::NT_CHAPTERS_LAST_ID;
            } elseif ($this->start_chapter_id >= Config::BOM_CHAPTERS_FIRST_ID && $this->start_chapter_id <= Config::BOM_CHAPTERS_LAST_ID) {
                $this->end_chapter_id = Config::BOM_CHAPTERS_LAST_ID;
            } elseif ($this->start_chapter_id >= Config::DC_CHAPTERS_FIRST_ID && $this->start_chapter_id <= Config::DC_CHAPTERS_LAST_ID) {
                $this->end_chapter_id = Config::DC_CHAPTERS_LAST_ID;
            } elseif ($this->start_chapter_id >= Config::PGP_CHAPTERS_FIRST_ID && $this->start_chapter_id <= Config::PGP_CHAPTERS_LAST_ID) {
                $this->end_chapter_id = Config::PGP_CHAPTERS_LAST_ID;
            }
        } else {
            $this->end_chapter_id = (int)$end_chapter_id;
        }
    }

    /********************************************************************/
    /*                       COMMON METHODS                             */
    /********************************************************************/

    /**
     * Validates and sets the $this->start_date variable
     *
     * @param string Start date of reading schedule.
     */
    private function set_start_date($start_date)
    {
        if (!$start_date) {
            $this->start_date = date('Y-m-d', strtotime(Config::DEFAULT_START_DATE));
        } else {
            $this->start_date = date('Y-m-d', strtotime($start_date));
        }
    }

    /**
     * Sets the $this->highlight_shortest_verse variable
     *
     * @param bool Whether or not to highlight shortest verse.
     */
    private function set_highlight_shortest_verse($highlight_shortest_verse)
    {
        if ($highlight_shortest_verse !== true && $highlight_shortest_verse !== false) {
            $this->highlight_shortest_verse = Config::DEFAULT_HIGHLIGHT_SHORTEST;
        } else {
            $this->highlight_shortest_verse = (bool)$highlight_shortest_verse;
        }
    }

    /**
     * Sets the $this->highlight_shortest_number variable
     *
     * @param int Number of verses to highlight.
     */
    private function set_highlight_shortest_number($highlight_shortest_number)
    {
        if (!is_int($highlight_shortest_number) || $highlight_shortest_number < 0 || $highlight_shortest_number > $this->total_verses) {
            $this->highlight_shortest_number = Config::DEFAULT_HIGHLIGHT_NUMBER;
        } else {
            $this->highlight_shortest_number = (int)$highlight_shortest_number;
        }
    }

    /**
     * Uses the scripture class to get the verse counts by chapter
     * Sets the $this->chapter_verse_count variable
     */
    private function set_chapter_verse_counts()
    {
        // Instantiate the scriptures class
        $scriptures = new Scriptures;
        // Total verses in the reading schedule
        $this->chapter_verse_counts = $scriptures->get_schedule_verse_counts($this->start_chapter_id, $this->end_chapter_id);
    }

    /**
     * Sets $this->total_verses variable
     */
    private function set_total_verses()
    {
        $verse_counts = null;
        foreach ($this->chapter_verse_counts as $chapter) {
            $verse_counts[] += $chapter['num_verses'];
        }
        $this->total_verses = array_sum((array)$verse_counts);
    }
}
