<?php
/**
 * Class to easily access Twig templating engine
 *
 * @author  Jared Howland <scriptures@jaredhowland.com>
 * @version 2019-11-12
 * @since   2012-12-06
 *
 */

namespace LDSScriptures;

use Twig\Environment;
use Twig\Extension\DebugExtension;
use Twig\Loader\FilesystemLoader;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class Template
{
    /**
     * Passes the content into the specified Twig template
     *
     * @param string $template Name of template to use
     * @param array  $content  Content to add to the template
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public static function display($template, $content)
    {
        $loader = new FilesystemLoader(dirname(__DIR__, 1).'/_templates');
        if (Config::DEVELOPMENT === true) {
            $twig = new Environment($loader, ['debug' => true]);
            $twig->addExtension(new DebugExtension());
        } else {
            $twig = new Environment($loader, array(
                'cache' => dirname(__DIR__, 1).'/_cache',
            ));
        }
        $twig->display($template, (array)$content);
    }
}
