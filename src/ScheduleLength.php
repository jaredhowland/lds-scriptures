<?php
/**
 * Class to create reading schedule using a length-based goal
 *
 * @author  Jared Howland <scriptures@jaredhowland.com>
 * @version 2019-11-19
 * @since   2012-12-14
 *
 */

namespace LDSScriptures;

use Exception;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class ScheduleLength extends Schedule
{
    private $chapters_or_verses; // Optional. Whether to break it up by chapters or verses per day. Valid values include 'chapters' and 'verses'.
    private $num_per_day; // Optional. Number of verses or chapters to read per day.
    private $date;
    private $end_date;

    /**
     * Schedule length constructor.
     *
     * @param array $args Array of arguments to construct the schedule length
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function __construct($args)
    {
        // Common variables to pass to parent constructor in schedule class
        $goal                      = $args['goal'];
        $start_chapter_id          = $args['start_chapter_id'];
        $end_chapter_id            = $args['end_chapter_id'];
        $start_date                = $args['start_date'];
        $highlight_shortest_verse  = $args['highlight_shortest_verse'];
        $highlight_shortest_number = $args['highlight_shortest_number'];
        // Length-based schedule variables
        $date               = $args['date'];
        $chapters_or_verses = $args['chapters_or_verses'];
        $num_per_day        = $args['num_per_day'];
        // Pass arguments to schedule class
        parent::__construct($goal, $start_chapter_id, $end_chapter_id, $start_date, $highlight_shortest_verse, $highlight_shortest_number);
        // Set variables for child class scheduleLength
        $this->set_chapters_or_verses($chapters_or_verses);
        $this->set_num_per_day($num_per_day);
        $this->set_end_date();
        $this->set_date($date);
    }

    /**
     * Create length-based reading schedule
     *
     * @return array Array containing metadata about the reading schedule.
     *
     * @throws Exception
     */
    public function length_based_schedule(): array
    {
        $scriptures = new Scriptures;
        $first_verse_id = Config::DEFAULT_START_VERSE_ID;
        $last_verse_id = Config::DEFAULT_END_VERSE_ID;

        if ($this->chapters_or_verses !== 'chapters' && $this->chapters_or_verses !== 'verses') {
            $this->chapters_or_verses = Config::DEFAULT_CHAPTERS_OR_VERSES;
        }

        // Chapters
        if ($this->chapters_or_verses === 'chapters') {
            $days_since_start = $this->calculate_days_between_dates($this->get_start_date(), $this->date);
            $first_chapter_id = $this->get_start_chapter_id() + ($this->num_per_day * ($days_since_start - 1));
            $last_chapter_id  = ($first_chapter_id + $this->num_per_day) - 1;
            if ($last_chapter_id > $this->get_end_chapter_id()) {
                $last_chapter_id = $this->get_end_chapter_id();
            }

            $first_verse_id = $scriptures->get_first_verse_id($first_chapter_id);
            $last_verse_id  = $scriptures->get_last_verse_id($last_chapter_id);
        }

        // Verses
        if ($this->chapters_or_verses === 'verses') {
            $days_since_start = $this->calculate_days_between_dates($this->get_start_date(), $this->date);
            $first_verse      = $scriptures->get_first_verse_id($this->get_start_chapter_id());
            $first_verse_id   = $first_verse + ($this->num_per_day * ($days_since_start - 1));
            $last_verse_id    = ($first_verse_id + $this->num_per_day) - 1;
            if ($last_verse_id > $scriptures->get_last_verse_id($this->get_end_chapter_id())) {
                $last_verse_id = $scriptures->get_last_verse_id($this->get_end_chapter_id());
            }
        }

        return [
            'next_url' => $this->get_next($this->date, $this->end_date).$this->get_next_prev_length(),
            'prev_url' => $this->get_prev($this->date, $this->end_date).$this->get_next_prev_length(),
            'url_schedule' => Config::url(),
            'url_date' => Config::url().'&amp;date='.$this->date,
            'update' => $this->get_atom_date($this->date),
            'schedule' => $this->get_schedule($scriptures, $first_verse_id, $last_verse_id),
        ];
    }

    /**
     * Get the reading schedule
     *
     * @param $scriptures     object Scriptures object
     * @param $first_verse_id int    First verse ID
     * @param $last_verse_id  int    Last verse ID
     *
     * @return array Array containing the reading schedule
     */
    private function get_schedule($scriptures, $first_verse_id, $last_verse_id): array
    {
        return $scriptures->reading($first_verse_id, $last_verse_id, $this->get_highlight_shortest_verse(), $this->get_highlight_shortest_number());
    }

    /**
     * Validates and sets the $this->chapters_or_verses variable
     *
     * @param string|null $chapters_or_verses Whether to create a schedule based on number of chapters or verses
     */
    private function set_chapters_or_verses($chapters_or_verses = null)
    {
        $this->chapters_or_verses = $chapters_or_verses;
        if ($this->chapters_or_verses === null || (($this->chapters_or_verses !== 'chapters') && ($this->chapters_or_verses !== 'verses'))) {
            $this->chapters_or_verses = Config::DEFAULT_CHAPTERS_OR_VERSES;
        }
    }

    /**
     * Validates and sets the $this->set_num_per_day variable
     *
     * @param int|null $num_per_day Number of verses or chapters to read per day
     */
    private function set_num_per_day($num_per_day = null)
    {
        $this->num_per_day = (int)$num_per_day;
        if (($this->num_per_day === null) || ($this->num_per_day < 1)) {
            // Verses
            if ($this->chapters_or_verses === 'verses') {
                if ($this->num_per_day > Config::PGP_VERSES_LAST_ID) {
                    $this->num_per_day = Config::DEFAULT_NUM_VERSES_PER_DAY;
                }
            // Chapters
            } elseif ($this->num_per_day > Config::PGP_CHAPTERS_LAST_ID) {
                $this->num_per_day = Config::DEFAULT_NUM_CHAPTERS_PER_DAY;
            }
        }
    }

    /**
     * Set the end date for the reading schedule
     */
    private function set_end_date()
    {
        if ($this->chapters_or_verses === 'chapters') {
            $chapters       = $this->get_total_chapters();
            $days           = (ceil($chapters / $this->num_per_day)) - 1;
            $this->end_date = date('Y-m-d', strtotime('+'.$days.' days', strtotime($this->get_start_date())));
        } elseif ($this->chapters_or_verses === 'verses') {
            $verses         = $this->get_total_verses();
            $days           = ceil($verses / $this->num_per_day);
            $this->end_date = date('Y-m-d', strtotime('+'.$days.' days', strtotime($this->get_start_date())));
        }
    }

    /**
     * Validates and sets the $this->date variable
     *
     * @param string|null $date Date of reading schedule. Valid values: a date in string format or `null`.
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    private function set_date($date = null)
    {
        if (empty($date)) {
            $date = date('Y-m-d', strtotime(Config::DEFAULT_DATE));
        }
        $this->date = date('Y-m-d', strtotime($date));
        if (strtotime($this->date) < strtotime($this->get_start_date())) {
            $error = 'It appears you are trying to see your reading schedule for '.date('F j, Y',
                        strtotime($this->date)).'. However, this schedule was not set to begin until '.date('F j, Y',
                        strtotime($this->get_start_date())).'.';
            Template::display('error.tmpl', [$error]);
            die();
        }

        if (strtotime($this->date) > strtotime($this->end_date)) {
            $error = 'It appears you are trying to see your reading schedule for '.date('F j, Y',
                        strtotime($this->date)).'. However, this schedule was set to end '.date('F j, Y',
                        strtotime($this->end_date)).'.';
            Template::display('error.tmpl', [$error]);
            die();
        }
    }

    /**
     * Gets total number of chapters in the reading schedule
     */
    private function get_total_chapters(): int
    {
        return ($this->get_end_chapter_id() - $this->get_start_chapter_id()) + 1;
    }

    /**
     * Get the next and previous lengths
     *
     * TODO: This should probably be refactored.
     */
    private function get_next_prev_length(): string
    {
        return '&amp;chapters_or_verses='.$this->chapters_or_verses.'&amp;num_per_day='.$this->num_per_day;
    }
}
