<?php
/**
 * Class to access scriptures in the SQLite database
 *
 * @author  Jared Howland <scriptures@jaredhowland.com>
 * @version 2019-11-18
 * @since   2012-12-06
 *
 */

namespace LDSScriptures;

use RuntimeException;

class Scriptures
{
    private $start_verse_id;
    private $end_verse_id;
    private $highlight_shortest_verse;
    private $highlight_shortest_number;

    private $verses;
    private $chapters;

    /**
     * Returns an array with the day’s reading schedule
     *
     * @param int  $start_verse_id
     * @param int  $end_verse_id
     * @param bool $highlight_shortest_verse
     * @param int  $highlight_shortest_number
     *
     * @return array Array containing the reading schedule
     *
     * @throws RuntimeException if start or end verse IDs are invalid
     */
    public function reading(
        $start_verse_id = Config::DEFAULT_START_VERSE_ID,
        $end_verse_id = Config::DEFAULT_END_VERSE_ID,
        $highlight_shortest_verse = Config::DEFAULT_HIGHLIGHT_SHORTEST,
        $highlight_shortest_number = Config::DEFAULT_HIGHLIGHT_NUMBER
    ): array {
        // Throw exception if an invalid verse id is used
        if ($end_verse_id < $start_verse_id) {
            throw new RuntimeException('Ending verse must be after starting verse. Start verse id: '.$start_verse_id.'; End verse id: '.$end_verse_id);
        }
        if ($start_verse_id < 1 || $end_verse_id > Config::PGP_VERSES_LAST_ID) {
            throw new RuntimeException('Verse IDs must be between '.Config::OT_VERSES_FIRST_ID.' and '.Config::PGP_VERSES_LAST_ID.' (last verse in database) inclusive. Start verse id: '.$start_verse_id.'; End verse id: '.$end_verse_id);
        }
        // Set variables
        $this->start_verse_id            = $start_verse_id;
        $this->end_verse_id              = $end_verse_id;
        $this->highlight_shortest_verse  = $highlight_shortest_verse;
        $this->highlight_shortest_number = $highlight_shortest_number;

        return $this->get_reading();
    }

    /**
     * Returns the verse id of the first verse of the chapter
     *
     * @param int Chapter id
     *
     * @return int Verse id of first verse in the chapter
     */
    public function get_first_verse_id($chapter_id): int
    {
        return (int)Config::db()->select('`id`')
                                ->from('`verses`')
                                ->where('`chapter_id` = :chapter_id')
                                ->orderBy('`id`')
                                ->limit('1')
                                ->bind(['chapter_id' => $chapter_id])
                                ->fetch()['id'];
    }

    /**
     * Returns the verse id of the last verse of the chapter
     *
     * @param int Chapter id
     *
     * @return int Verse id of last verse in the chapter
     */
    public function get_last_verse_id($chapter_id): int
    {
        return (int)Config::db()->select('`id`')
                                ->from('`verses`')
                                ->where('`chapter_id` = :chapter_id')
                                ->orderBy('`id` DESC')
                                ->limit('1')
                                ->bind(['chapter_id' => $chapter_id])
                                ->fetch()['id'];
    }

    /**
     * Returns the count of all verses in the reading schedule
     * This differs from get_verse_count in that it is the full schedule
     * and not just the schedule for one day
     *
     * @param int ID of first chapter
     * @param int ID of last chapter
     *
     * @return array Array of the verse count for every chapter in the reading schedule
     */
    public function get_schedule_verse_counts($start_chapter_id, $end_chapter_id): array
    {
        return Config::db()->select('`chapter_id`, COUNT(`id`) as `num_verses`')
                           ->from('`verses`')
                           ->where('`chapter_id` BETWEEN :start_chapter_id AND :end_chapter_id')
                           ->groupBy('`chapter_id`')
                           ->bind(['start_chapter_id' => $start_chapter_id, 'end_chapter_id' => $end_chapter_id])
                           ->fetchAll();
    }

    /**
     * Get reading assignment
     *
     * @return array Array of reading assignment
     */
    private function get_reading(): array
    {
        $this->verses   = $this->get_verses();
        $this->chapters = $this->get_chapters();
        // Calculate the shortest verse only if $highlight_shortest_verse is TRUE
        if ($this->highlight_shortest_verse && $this->highlight_shortest_number > 0) {
            $shortest_verse_id = $this->group_verses($this->highlight_shortest_number);
        } else {
            $shortest_verse_id = 0;
        }

        return [
            'reading' => $this->get_chapter_titles(),
            'verse_count' => $this->get_verse_count(),
            'word_count' => $this->get_word_count(),
            'est_time' => $this->get_est_time(),
            'highlight_shortest' => $this->highlight_shortest_verse,
            'shortest_verse_id' => $shortest_verse_id,
            'highlight_shortest_number' => $this->highlight_shortest_number,
            'chapters' => $this->chapters,
        ];
    }

    /**
     * Returns a string containing the titles for day’s reading
     * *Example:* '1 Nephi 2–1 Nephi 4' or 'Leviticus 11'
     *
     * @param int ID of the first verse
     * @param int ID of the last verse
     *
     * @return string Titles for day’s reading
     */
    private function get_chapter_titles(): string
    {
        $keys        = array_keys($this->chapters);
        $end_title   = $this->get_chapter_title(end($keys));
        $start_title = $this->get_chapter_title(reset($keys));
        if ($start_title === $end_title) {
            return $start_title;
        }

        return $start_title.'–'.$end_title;
    }

    /**
     * Returns a string containing the chapter title
     * *Example:* '1 Nephi 2' or 'Leviticus 11'
     *
     * @param int Chapter ID
     *
     * @return string Chapter title
     */
    private function get_chapter_title($chapter_id): string
    {
        return $this->chapters[$chapter_id]['title'];
    }

    /**
     * Groups verses into number of verses to highlight and returns first verse_id of smallest section to highlight
     *
     * @param int Number of verses to highlight
     *
     * @return int verse_id of first verse in the highlighted section
     */
    private function group_verses($num_verses): int
    {
        $verses = $this->verses;
        foreach ($verses as $key => $verse) {
            $slice = array_slice($verses, $key, $num_verses, true);
            if (count($slice) === $num_verses) {
                foreach ($slice as $scripture) {
                    $strlen[] = strlen($scripture['scripture']);
                    $id       = $scripture['id'];
                }
                $start_id  = ($id - $num_verses) + 1;
                $reading[] = ['start_id' => $start_id, 'length' => array_sum($strlen)];
                $strlen    = null;
            }
        }

        return $this->min_with_key($reading, 'length');
    }

    /**
     * Finds min value from multi-dimensional array
     * http://stackoverflow.com/questions/4497810/min-and-max-in-multidimensional-array
     *
     * @param array  Array to search for the min value
     * @param string Name of key in multi-dimensional array you need min value for
     *
     * @return int ID pulled from $array
     */
    private function min_with_key($array, $key): int
    {
        if (!is_array($array) || count($array) === 0) {
            return false;
        }
        $min = $array[0][$key];
        $id  = $array[0]['start_id'];
        foreach ($array as $a) {
            if ($a[$key] < $min) {
                $min = $a[$key];
                $id  = $a['start_id'];
            }
        }

        return $id;
    }

    /**
     * Get verse count
     *
     * @return int Number of verses in day’s reading
     */
    private function get_verse_count(): int
    {
        return count($this->extract_verses());
    }

    /**
     * Get word count
     *
     * @return int Number of words in day’s reading
     */
    private function get_word_count(): int
    {
        return str_word_count(implode(' ', $this->extract_verses()));
    }

    /**
     * Get estimated reading time
     *
     * @return int Estimated reading time in minutes
     */
    private function get_est_time(): int
    {
        return number_format(ceil($this->get_word_count() / Config::WORDS_PER_MINUTE));
    }

    /**
     * Returns an array with just the verse text (each verse is an array value)
     *
     * @return array Array containing verses
     */
    private function extract_verses(): array
    {
        return array_map(static function ($ar) {
            return $ar['scripture'];
        }, $this->verses); // http://stackoverflow.com/a/7994555
    }

    /**
     * Returns an array grouped by chapter
     *
     * @return array Array containing verses grouped by chapter id with chapter metadata
     */
    private function get_chapters(): array
    {
        // Group verses by chapter id
        $all_verses = $this->group_array($this->verses, 'chapter_id');
        foreach ($all_verses as $chapter_id => $verses) {
            // Shorten Doctrine & Covenants
            $book = $verses[0]['book'];
            if ($book === 'Doctrine & Covenants') {
                $book = 'D&C';
            }
            $summary    = $this->fix_small_caps($verses[0]['chapter_summary']);
            $summary    = $this->fix_numbers($summary);
            $chapters[] = [
                'id' => $chapter_id,
                'title' => $book.' '.$verses[0]['chapter'],
                'summary' => $summary,
                'verses' => $verses,
            ];
        }

        return $chapters;
    }

    /**
     * Returns an array with the day’s verses
     *
     * @return array Array containing database query results of verses
     */
    private function get_verses(): array
    {
        return Config::db()->select('`v`.`id`, `b`.`title` AS `book`, `c`.`id` AS `chapter_id`, `c`.`chapter`, `lc`.`chapter_summary`, `v`.`verse`, `v`.`scripture`')
                           ->from('`verses` `v`')
                           ->innerJoin('`chapters` `c`')
                           ->on('`v`.`chapter_id` = `c`.`id`')
                           ->innerJoin('`lds_chapters` `lc`')
                           ->on('`c`.`id` = `lc`.`id`')
                           ->innerJoin('`books` `b`')
                           ->on('`c`.`book_id` = `b`.`id`')
                           ->where('`v`.`id` BETWEEN :start_verse_id AND :end_verse_id')
                           ->orderBy('`v`.`id`')
                           ->bind(['start_verse_id' => $this->start_verse_id, 'end_verse_id' => $this->end_verse_id])
                           ->fetchAll();
    }

    /**
     * Encapsulates B.C. and A.D. in <span> with class 'caps' for small caps display
     *
     * @param string Chapter summary
     *
     * @return string String containing corrected small-caps for B.C. and A.D. in chapter summaries
     */
    private function fix_small_caps($summary): string
    {
        // Fix B.C and A.D. to be small caps
        $search  = ['B.C.', 'A.D.'];
        $replace = ['<span class="caps">B.C.</span>', '<span class="caps">A.D.</span>'];

        return str_replace($search, $replace, $summary);
    }

    /**
     * Encapsulates numbers in <span> with class 'num' for styling numbers
     *
     * @param string String to check for numbers to format
     *
     * @return string String with formatted numbers for styling
     */
    private function fix_numbers($summary): string
    {
        return preg_replace('/(\d+)/', '<span class="num">$1</span>', $summary);
    }

    /**
     * Returns an array grouped by provided array index
     *
     * @param array      Array to group by given index (`$array_index`)
     * @param string|int Array index key to group the array by
     *
     * @return array Array containing verses grouped by chapter id
     */
    private function group_array($array, $array_index): array
    {
        $output = null;
        foreach ($array as $key => $value) {
            $output[$value[$array_index]][] = $value;
        }

        return $output;
    }
}
