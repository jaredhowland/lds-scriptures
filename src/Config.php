<?php
/**
 * Configuration class.
 *
 * @author  Jared Howland <scriptures@jaredhowland.com>
 * @version 2019-11-19
 * @since   2012-12-05
 *
 */

namespace LDSScriptures;

use Database\Database;

class Config
{
    // Variables
    private static $db;

    // Constants
    // App metadata
    const DEVELOPMENT = true; // Changes the app behavior (error reporting, template caching, which database to use, etc.)
    const WORDS_PER_MINUTE = 300; // Average college graduate can read ~300 words per minute. Used to calculate estimated reading times.

    // Reading schedule defaults
    // All constants ending in DATE rely on the PHP strtotime() function
    // Valid values for those constants can be found in the PHP documentation
    // http://php.net/manual/en/function.strtotime.php
    // If invalid values are entered, will always fall back to these defaults
    const TIME_ZONE = 'America/Denver'; // Needed for date calculations in PHP
    const DEFAULT_FORMAT = 'html'; // Default format for the reading schedule. Possible values are 'html', 'atom', and 'json'.
    const DEFAULT_START_DATE = 'Today';  // Default date to start the reading schedule
    const DEFAULT_END_DATE = '+6 months'; // Default target end date – always relative to DEFAULT_START_DATE constant
    const DEFAULT_DATE = 'Today'; // Default day to show for reading schedule
    const DEFAULT_HIGHLIGHT_SHORTEST = false;
    const DEFAULT_HIGHLIGHT_NUMBER = 1;
    const DEFAULT_START_CHAPTER_ID = 1190; // First chapter in Book of Mormon
    const DEFAULT_END_CHAPTER_ID = 1428; // Last chapter in Book of Mormon
    const DEFAULT_START_VERSE_ID = 31103; // First verse in Book of Mormon
    const DEFAULT_END_VERSE_ID = 37706; // Last verse in Book of Mormon
    const DEFAULT_GOAL = 'time'; // Default goal type. Possible values are 'time' and 'length'.
    const DEFAULT_CHAPTERS_OR_VERSES = 'chapters'; // If goal is length based, break schedule by chapters per day. Possible values are 'chapters' and 'verses'.
    const DEFAULT_NUM_CHAPTERS_PER_DAY = 1; // If goal is length based, the number of chapters to read per day
    const DEFAULT_NUM_VERSES_PER_DAY = 30; // If goal is length based, the number of verses to read per day

    // Databases
    // No username/password required for SQLite database connection
    // App dynamically sets the appropriate database based on state of DEVELOPMENT constant
    const DEV_DB_PATH = 'dev.db'; // Path to development database
    const PROD_DB_PATH = 'scriptures.db'; // Path to production database


    /****************************************************************************/
    /*                       DO NOT EDIT BELOW THIS LINE                        */
    /****************************************************************************/

    // Database IDs for various scripture entry points
    // Allows for reading schedule calculations without multiple database lookups

    // Old Testament
    // books
    //     ids: 1–39
    //   count: 39
    // chapters
    //     ids: 1–929
    //   count: 929
    // verses
    //     ids: 1–23145
    //   count: 23145
    const OT_BOOKS_NUM = 39;
    const OT_BOOKS_FIRST_ID = 1;
    const OT_BOOKS_LAST_ID = 39;
    const OT_CHAPTERS_NUM = 929;
    const OT_CHAPTERS_FIRST_ID = 1;
    const OT_CHAPTERS_LAST_ID = 929;
    const OT_VERSES_NUM = 23145;
    const OT_VERSES_FIRST_ID = 1;
    const OT_VERSES_LAST_ID = 23145;

    // New Testament
    // books
    //     ids: 40–66
    //   count: 27
    // chapters
    //     ids: 930–1189
    //   count: 260
    // verses
    //     ids: 23146–31102
    //   count: 7957
    const NT_BOOKS_NUM = 27;
    const NT_BOOKS_FIRST_ID = 40;
    const NT_BOOKS_LAST_ID = 66;
    const NT_CHAPTERS_NUM = 260;
    const NT_CHAPTERS_FIRST_ID = 930;
    const NT_CHAPTERS_LAST_ID = 1189;
    const NT_VERSES_NUM = 7957;
    const NT_VERSES_FIRST_ID = 23146;
    const NT_VERSES_LAST_ID = 31102;

    // Book of Mormon
    // books
    //     ids: 67–81
    //   count: 15
    // chapters
    //     ids: 1190–1428
    //   count: 239
    // verses
    //     ids: 31103–37706
    //   count: 6604
    const BOM_BOOKS_NUM = 15;
    const BOM_BOOKS_FIRST_ID = 67;
    const BOM_BOOKS_LAST_ID = 81;
    const BOM_CHAPTERS_NUM = 239;
    const BOM_CHAPTERS_FIRST_ID = 1190;
    const BOM_CHAPTERS_LAST_ID = 1428;
    const BOM_VERSES_NUM = 6604;
    const BOM_VERSES_FIRST_ID = 31103;
    const BOM_VERSES_LAST_ID = 37706;

    // Doctrine and Covenants
    // books
    //     ids: 82
    //   count: 1
    // chapters
    //     ids: 1429–1566
    //   count: 138
    // verses
    //     ids: 37707–41360
    //   count: 3654
    const DC_BOOKS_NUM = 1;
    const DC_BOOKS_FIRST_ID = 82;
    const DC_BOOKS_LAST_ID = 82;
    const DC_CHAPTERS_NUM = 138;
    const DC_CHAPTERS_FIRST_ID = 1429;
    const DC_CHAPTERS_LAST_ID = 1566;
    const DC_VERSES_NUM = 3654;
    const DC_VERSES_FIRST_ID = 37707;
    const DC_VERSES_LAST_ID = 41360;

    // Pearl of Great Price
    // books
    //     ids: 83–87
    //   count: 5
    // chapters
    //     ids: 1567–1582
    //   count: 16
    // verses
    //     ids: 41361–41995
    //   count: 635
    const PGP_BOOKS_NUM = 5;
    const PGP_BOOKS_FIRST_ID = 83;
    const PGP_BOOKS_LAST_ID = 87;
    const PGP_CHAPTERS_NUM = 16;
    const PGP_CHAPTERS_FIRST_ID = 1567;
    const PGP_CHAPTERS_LAST_ID = 1582;
    const PGP_VERSES_NUM = 635;
    const PGP_VERSES_FIRST_ID = 41361;
    const PGP_VERSES_LAST_ID = 41995;

    /**
     * Determines which database path to use
     * Based on state of DEVELOPMENT constant
     *
     * @param null
     *
     * @return string Path to database
     */
    public static function db_path(): string
    {
        if (self::DEVELOPMENT) {
            return self::DEV_DB_PATH;
        }

        return self::PROD_DB_PATH;
    }

    /**
     * Create database connection to SQLite file
     */
    public static function db()
    {
        if(self::$db === null) {
            $database = new Database;
            self::$db = $database->
                        driver('sqlite')->
                        dbPath(self::db_path())->
                        connect();
        }

        return self::$db;
    }

    /**
     * Determines type of error reporting
     * Based on state of DEVELOPMENT constant
     *
     * @param null
     *
     * @return string Type of error reporting
     */
    public static function set_error_reporting()
    {
        // Always report an error (but not always to end user)
        ini_set('error_reporting', E_ALL);
        if (self::DEVELOPMENT) {
            ini_set('display_errors', 1);
        } else {
            // Log all errors but do not display them to the end user
            ini_set('display_errors', 0);
            ini_set('log_errors', 1);
        }
    }

    /**
     * Get full URL of current page (including query string)
     *
     * @link https://stackoverflow.com/a/37071036
     *
     * @return string Full URL
     */
    public static function url(): string
    {
        // Output: Array ( [dirname] => /myproject [basename] => index.php [extension] => php [filename] => index )
        $pathInfo = pathinfo($_SERVER['PHP_SELF']);

        return '//'.$_SERVER['HTTP_HOST'].$pathInfo['dirname'].$pathInfo['basename'].'?'.$_SERVER['QUERY_STRING'];
    }

} // End class

/****************************************/
/* Miscellaneous configuration settings */
/****************************************/

// Set default time zone
date_default_timezone_set(Config::TIME_ZONE);

// Set error reporting
Config::set_error_reporting();
