<?php
/**
 * Class to create reading schedule
 *
 * @author  Jared Howland <scriptures@jaredhowland.com>
 * @version 2019-11-12
 * @since   2012-12-08
 *
 */

namespace LDSScriptures;

use Exception;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class Logic
{
    // Define variables. If not provided, default values are used from the Config file.
    private $goal; // Optional. Valid values include 'time' and 'length'.
    private $format; // Format of schedule. Valid values include 'html', 'atom', and 'json'.

    /**
     * Create the reading schedule object
     *
     * @param array $args Array of arguments to set up reading schedule
     */
    public function create($args)
    {
        $this->set_format($args['format']);
        $this->set_goal($args['goal']);

        if ($this->goal === 'time') {
            try {
                $schedule = new ScheduleTime($args);
                $reading  = $schedule->time_based_schedule();
                $this->generate_template($reading);
            } catch (Exception $e) {
                echo 'Error: ', $e->getMessage();
            }
        }

        if ($this->goal === 'length') {
            try {
                $schedule = new ScheduleLength($args);
                $reading  = $schedule->length_based_schedule();
                $this->generate_template($reading);
            } catch (Exception $e) {
                echo 'Error: ', $e->getMessage();
            }
        }
    }

    /**
     * Create and display the data in a Twig template
     *
     * @param array $schedule Array containing metadata about the reading schedule
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    private function generate_template($schedule)
    {
        switch ($this->format) {
            case 'html':
                header('Content-type: text/html; charset=utf-8');
                Template::display('html.tmpl', $schedule);
                break;
            case 'atom':
                header('Content-type: application/atom+xml; charset=utf-8');
                Template::display('atom.tmpl', $schedule);
                break;
            case 'json':
                header('Content-type: application/json; charset=utf-8');
                echo json_encode($schedule);
                break;
            default:
                Template::display('error.tmpl', ['You may have entered an invalid format. Please go back and try again.']);
                die();
                break;
        }
    }

    /**
     * Sets the $this->format variable
     *
     * @param string Format of returned reading schedule. Valid values include 'html', 'atom', and 'json'.
     *
     */
    private function set_format($format = null)
    {
        if ($format === null || ($format !== 'html' && $format !== 'atom' && $format !== 'json')) {
            $this->format = Config::DEFAULT_FORMAT;
        } else {
            $this->format = $format;
        }
    }

    /**
     * Sets the $this->goal variable
     *
     * @param string Reading goal. Valid values are 'time' and 'length'.
     *
     */
    private function set_goal($goal = null)
    {
        // Fall back to default goal if not set correctly
        if ($goal !== 'time' && $goal !== 'length') {
            $this->goal = Config::DEFAULT_GOAL;
        } else {
            $this->goal = $goal;
        }
    }
}
