<?php

namespace LDSScriptures;

use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

require_once __DIR__.'/vendor/autoload.php';

// World’s Ugliest UI ™, ®, ©

/**
 * Return form for selecting the chapter id for creating the schedule
 *
 * @param string $name Name of form section to create. Valid values: `start` or `end`
 *
 * @return string Form section
 */
function chapter_form($name)
{
    $default_id = $name === 'start' ? Config::DEFAULT_START_CHAPTER_ID : Config::DEFAULT_END_CHAPTER_ID;
    $form       = null;
    for ($chapter_id = Config::OT_CHAPTERS_FIRST_ID; $chapter_id <= Config::PGP_CHAPTERS_LAST_ID; $chapter_id++) {
        if ($chapter_id === $default_id) {
            $form .= '<option value="'.$chapter_id.'" selected="selected">'.$chapter_id.'</option>';
        }
        $form .= '<option value="'.$chapter_id.'">'.$chapter_id.'</option>';
    }

    return $form;
}

$start_chapter_id_form   = chapter_form('start');
$end_chapter_id_form     = chapter_form('end');
$chapters_or_verses_form = null;

if (Config::DEFAULT_CHAPTERS_OR_VERSES === 'chapters') {
    $chapters_or_verses_form .= '<option value="chapters" selected="selected">Chapters</option><option value="verses">Verses</option>';
    $num_per_day             = Config::DEFAULT_NUM_CHAPTERS_PER_DAY;
} else {
    $chapters_or_verses_form .= '<option value="chapters">Chapters</option><option value="verses" selected="selected">Verses</option>';
    $num_per_day             = Config::DEFAULT_NUM_VERSES_PER_DAY;
}

if (Config::DEFAULT_HIGHLIGHT_SHORTEST === true) {
    $highlight_form = '<input type="checkbox" name="highlight_shortest_verse" value="true" id="highlight_shortest_verse" checked="checked">';
} else {
    $highlight_form = '<input type="checkbox" name="highlight_shortest_verse" value="true" id="highlight_shortest_verse">';
}

$start_date = date('Y-m-d', strtotime(Config::DEFAULT_START_DATE));
$end_date   = date('Y-m-d', strtotime(Config::DEFAULT_END_DATE));

$html = <<<HTML
<h1>Time-based Schedule</h1>
<form action="schedule.php" method="get" accept-charset="utf-8">
  <input type="hidden" name="goal" value="time" id="goal">
  <p><label for="start_chapter_id">Start Chapter ID: </label>
  <select name="start_chapter_id" id="start_chapter_id">
    $start_chapter_id_form
  </select>
  <p><label for="end_chapter_id">End Chapter ID: </label>
  <select name="end_chapter_id" id="end_chapter_id">
    $end_chapter_id_form
  </select>
  <p><label for="start_date">Start Date: </label><input type="date" name="start_date" value="$start_date" id="start_date"></p>
  <p><label for="end_date">End Date: </label><input type="date" name="end_date" value="$end_date" id="end_date"></p>
  <label for="highlight_shortest_verse">Highlight Shortest Verse: </label>$highlight_form
  <p><input type="submit" value="Continue &rarr;"></p>
</form>

<h1>Length-based Schedule</h1>
<form action="schedule.php" method="get" accept-charset="utf-8">
  <input type="hidden" name="goal" value="length" id="goal">
  <p><label for="chapters_or_verses">Read Whole Chapters or Set Number of Verses: </label>
  <select name="chapters_or_verses" id="chapters_or_verses">
    $chapters_or_verses_form
  </select></p>
  <label for="num_per_day">Number Per Day: </label><input type="text" name="num_per_day" value="$num_per_day" id="num_per_day">
  <p><label for="start_chapter_id">Start Chapter ID: </label>
  <select name="start_chapter_id" id="start_chapter_id">
    $start_chapter_id_form
  </select>
  <p><label for="end_chapter_id">End Chapter ID: </label>
  <select name="end_chapter_id" id="end_chapter_id">
    $end_chapter_id_form
  </select>
  <p><label for="start_date">Start Date: </label><input type="date" name="start_date" value="$start_date" id="start_date"></p>
  <label for="highlight_shortest_verse">Highlight Shortest Verse: </label>$highlight_form
  <p><input type="submit" value="Continue &rarr;"></p>
</form>
HTML;
$content = array('content' => $html);

try {
    Template::display('generic.tmpl', $content);
} catch (LoaderError $e) {
    echo "Error: $e";
} catch (RuntimeError $e) {
    echo "Error: $e";
} catch (SyntaxError $e) {
    echo "Error: $e";
}
